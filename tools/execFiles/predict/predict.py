
#### Imports, File Names and Plot Parameters
import caffe
import sys
import os
import numpy as np
import pandas as pd

#print len(sys.argv)
if len(sys.argv)!=6:
	print "Wrong number of arguments. Need:"
	print "1) Deploy file"
	print "2) Caffe Model"
	print "3) Images Folder"
	print "4) Mean File"
	print "5) Synset File"
	sys.exit(0)

basePath=os.getcwd()
print "Current directory:"
print basePath

print "Deploy file:"
print basePath+sys.argv[1]
DEPLOY_FILE  = basePath+sys.argv[1]

print "CaffeModel file:"
print basePath+sys.argv[2]
MODEL_FILE  = basePath+sys.argv[2]

print "Images Folder:"
print basePath+sys.argv[3]
IMAGE_FILES  = basePath+sys.argv[3]

print "Mean file:"
print basePath+sys.argv[4]
MEAN_FILE  = basePath+sys.argv[4]

print "Synset file:"
print basePath+sys.argv[5]
SYNSET_FILE  = basePath+sys.argv[5]

if not os.path.isfile(MODEL_FILE):
    print("MODEL_FILE File not found")
    sys.exit(0)
else:
    print("MODEL_FILE seems fine.")


if not os.path.isfile(DEPLOY_FILE):
    print("DEPLOY_FILE File not found")
    sys.exit(0)
else:
    print("DEPLOY_FILE seems fine.")

if not os.path.isdir(IMAGE_FILES):
    print("IMAGE_FILES folder not found")
    sys.exit(0)
else:
    print("IMAGE_FILES seems fine.")

if not os.path.isfile(MEAN_FILE):
    print("MEAN_FILE File not found")
    sys.exit(0)
else:
    print("MEAN_FILE seems fine.")


if not os.path.isfile(SYNSET_FILE):
    print("SYNSET_FILE File not found")
    sys.exit(0)
else:
    print("SYNSET_FILE seems fine.")

	
#####Load labels
with open(SYNSET_FILE) as f:
	labels_df = pd.DataFrame([
		{
			'synset_id': (l.strip().split(' ')[0]),
			'name': ' '.join(l.strip().split(' ')[1:]).split(',')[0]
			}
		for l in f.readlines()
	])	
synset = labels_df.sort('synset_id')['name'].values

print synset

##### Initialize Caffe
caffe.set_mode_gpu()


##### Prediction Variante 2
net = caffe.Classifier(
            DEPLOY_FILE, MODEL_FILE,
            image_dims=(256, 256),
            raw_scale=255,
            mean=np.load(MEAN_FILE).mean(1).mean(1),
            channel_swap=(2, 1, 0)
        )
print
print "Prediction"

for i in os.listdir(IMAGE_FILES):
    if i.endswith(".jpg") or i.endswith(".png"): 
        scores = net.predict([caffe.io.load_image(IMAGE_FILES + "/" + i)], oversample=True)
        print "scores " + str(scores)
        predictionIndex = scores.argmax()
        print "predict image " + str(i)
        print "prediction as index:  " + str(predictionIndex)
        print "prediction as word:  " + synset[predictionIndex]
        print "probability for that" + str(scores.max())
        print 
        continue

##### Prediction Variante 1

# net = caffe.Net(DEPLOY_FILE,
                # MODEL_FILE,
                # caffe.TEST)

# transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
# transformer.set_transpose('data', (2,0,1))
# transformer.set_mean('data', np.load(MEAN_FILE).mean(1).mean(1)) # mean pixel
# transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
# transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB

# print
# print "Prediction"
# net.blobs['data'].data[...] = transformer.preprocess('data', caffe.io.load_image(IMAGE_FILE))
# out = net.forward()
# print "as index:"
# predictionIndex=out['prob'].argmax()
# print predictionIndex
# print "as word:"
# predictionWord = synset[predictionIndex]
# print predictionWord





