#Call from /shared/nn_master_project/

NETWORK_DIR=/michael/server/static/caffe_models/bottles12_2
deploy_path=$NETWORK_DIR/deploy.prototxt
cmodel_path=$NETWORK_DIR/model.caffemodel
mean_path=$NETWORK_DIR/mean.npy
synset_path=$NETWORK_DIR/synset.txt
imgfile_path=/jonas/data/bottles-12/validation

python ./jonas/tools/customTools/predict/predict.py $deploy_path $cmodel_path $imgfile_path $mean_path $synset_path
