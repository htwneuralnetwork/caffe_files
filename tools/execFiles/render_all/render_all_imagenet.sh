NETWORK_DIR=/shared/studenten/nn_master_project/michael/server/static/caffe_models/lenet
deploy_path=$NETWORK_DIR/deploy.prototxt
cmodel_path=$NETWORK_DIR/model.caffemodel
imgfile_path=/shared/studenten/nn_master_project/joshua/imagenetPics/mate.jpg
imgDir_path=/shared/studenten/nn_master_project/joshua/imagenetPics/rendered/


python ./render_all_abs_path.py $deploy_path $cmodel_path $imgfile_path $imgDir_path
