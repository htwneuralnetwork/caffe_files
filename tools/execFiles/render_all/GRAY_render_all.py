
#### Imports, File Names and Plot Parameters
import skimage
import caffe
import sys
import os
import numpy as np
import numpy
import matplotlib.pyplot as plt
#%matplotlib inline
import json

plt.rcParams['figure.figsize'] = (10, 10)
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

#print len(sys.argv)
if len(sys.argv)!=5:
	print "Wrong number of arguments. Need:"
	print "1) Deploy file"
	print "2) Caffe Model"
	print "3) Image File"
	print "4) Name of output subdirectory"
	sys.exit(0)

basePath=os.getcwd()
print "Current directory:"
print basePath

print "Deploy file:"
print basePath+sys.argv[1]
DEPLOY_FILE  = basePath+sys.argv[1]

print "CaffeModel file:"
print basePath+sys.argv[2]
PRETRAINED  = basePath+sys.argv[2]

print "Image file:"
print basePath+sys.argv[3]
IMAGE_FILE  = basePath+sys.argv[3]

print "Output folder:"
print basePath+sys.argv[4]
saveToBasePath  = basePath+sys.argv[4]


if not os.path.isfile(PRETRAINED):
    print("PRETRAINED File not found")
else:
    print("PRETRAINED seems fine.")
    
if not os.path.isfile(DEPLOY_FILE):
    print("DEPLOY_FILE File not found")
else:
    print("DEPLOY_FILE seems fine.")
    
if not os.path.isfile(IMAGE_FILE):
    print("IMAGE_FILE File not found")
else:
    print("IMAGE_FILE seems fine.")


##### Initialize Caffe
caffe.set_mode_cpu()
net = caffe.Classifier(DEPLOY_FILE, PRETRAINED) #, image_dims=(28, 28)

##### Layer names and Blob sizes
feature_shape = [(k, v.data.shape) for k, v in net.blobs.items()]
print 
print "Feature shapes:"
print feature_shape

##### Parameter shapes
parameter_shape = [(k, v[0].data.shape) for k, v in net.params.items()]
print
print "Parameter shapes:"
print parameter_shape

##### Transforming of input
inputLayerName = feature_shape[0][0];
transformer = caffe.io.Transformer({inputLayerName: net.blobs[inputLayerName].data.shape})
transformer.set_transpose(inputLayerName, (2,0,1))
##transformer.set_mean('data', np.load(caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1)) # mean pixel
transformer.set_raw_scale(inputLayerName, 255)  # the reference model operates on images in [0,255] range instead of [0,1]
transformer.set_channel_swap(inputLayerName, (2,1,0))  # the reference model has channels in BGR order instead of RGB

##### Forward pass
print
print "Forward pass"
net.blobs[inputLayerName].data[...] = transformer.preprocess(inputLayerName, caffe.io.load_image(IMAGE_FILE))
net.forward()


##### Print Prediction
number_of_layers = len(feature_shape)

print "Last layer for prediction:"
layerName = feature_shape[number_of_layers-1][0];
print layerName
     
feature = net.blobs[layerName].data[0][:36]
print "Output Layer is:"
print feature
print "Prediction is:"
print feature.argmax()








####### Save images
### Function for creating dirs
def createDirIfNotExists(name):
    if not os.path.exists(name):
        os.makedirs(name)



##### Save filters
import scipy
filterBasePath=saveToBasePath+'/filters/'

print "Get and save Filters"
print "Trying to save filters to following directory:"
print filterBasePath

number_of_layers = len(parameter_shape)
filter = None
for i in range(0, number_of_layers):
	typeOfLayer = len(parameter_shape[i][1])
	layerName = parameter_shape[i][0]
	print "LayerName:"
	print layerName
	print typeOfLayer
	#Is convolutional Layer?
	if typeOfLayer == 4:
		filters = net.params[layerName][0].data
		#print filters.shape

		###Swap dimensions (by transposition)
		filtersTransposed = filters.transpose(0, 2, 3, 1)
		#print filtersTransposed.shape

		###Delete last axis (channel) for grayscale
		filtersTransposed = filtersTransposed[:,:,:,:1]
		filtersTransposed = np.squeeze(filtersTransposed, axis=(3,))
		#print filtersTransposed.shape
		numberOfFilters = len(filtersTransposed)
		filterPath=filterBasePath+layerName+"/"
		createDirIfNotExists(filterPath)
		for i in range(0, numberOfFilters):
		    scipy.misc.imsave(filterPath+str(i)+'.png', filtersTransposed[i])

print "Saving of filters was successful"



##### Save Features

###Function for creating two dimensional array out of vector
def create2dimArr(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]        

featurePath=saveToBasePath+'/features/'
print "Trying to save features to following directory:"
print featurePath

number_of_layers = len(feature_shape)

for i in range(0, number_of_layers):
    layerName = feature_shape[i][0];
    feature = net.blobs[layerName].data[0]
    print feature.shape
    typeOfLayer=len(feature.shape)
    if typeOfLayer == 3:
        print len(feature)
        for j in range(0, len(feature)):
            createDirIfNotExists(featurePath+layerName)
            scipy.misc.imsave(featurePath+layerName+'/'+str(j)+'.png', feature[j])
    if typeOfLayer == 1:
        #print len(feature)
        #print feature.max()        
        if(feature.max()<=1.1):
            #createDirIfNotExists(featurePath+layerName)
            #print layerName
            #print feature
            grayscale = feature*255
            print grayscale
            newArr=create2dimArr(grayscale,10)
            scipy.misc.imsave(featurePath+layerName+'.png', newArr)

print "Saving of features was successful"

