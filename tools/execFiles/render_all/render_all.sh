NETWORK_DIR=/examples/bottles_mean_fcplus
deploy_path=$NETWORK_DIR/deploy.prototxt
cmodel_path=$NETWORK_DIR/output/run_2/snapshots/_iter_20000.caffemodel
imgfile_path=/data/bottles/Mate/0_0303.jpg
imgDir_path=$NETWORK_DIR/output/run_1/images/


python ./tools/customTools/render_all/render_all.py $deploy_path $cmodel_path $imgfile_path $imgDir_path
