#$1 Pfad zur deploy.prototxt
#$2 Pfad zur iter_???.caffemodel
#$3 Pfad zu einem Bild fuer das Netzwerk
#$4 Pfad zum images ordner

NETWORK_DIR=/examples/Bottles_v2
deploy_path=$NETWORK_DIR/deploy.prototxt
cmodel_path=$NETWORK_DIR/output/run_1/snapshots/_iter_1000.caffemodel
imgfile_path=/data/bottles/Mate/0_0303.jpg
imgDir_path=$NETWORK_DIR/output/run_1/images/


python ./tools/customTools/render_all/render_all.py $deploy_path $cmodel_path $imgfile_path $imgDir_path
