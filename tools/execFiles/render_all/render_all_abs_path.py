
#### Imports, File Names and Plot Parameters
import skimage
import caffe
import sys
import os
import numpy as np
import numpy
import matplotlib.pyplot as plt
#%matplotlib inline
import json

plt.rcParams['figure.figsize'] = (10, 10)
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

#print len(sys.argv)
if len(sys.argv)!=5:
	print "Wrong number of arguments. Need:"
	print "1) Deploy file"
	print "2) Caffe Model"
	print "3) Image File"
	print "4) Name of output subdirectory"
	sys.exit(0)

basePath=""#os.getcwd()
print "Current directory:"
print basePath

print "Deploy file:"
print basePath+sys.argv[1]
DEPLOY_FILE  = basePath+sys.argv[1]

print "CaffeModel file:"
print basePath+sys.argv[2]
PRETRAINED  = basePath+sys.argv[2]

print "Image file:"
print basePath+sys.argv[3]
IMAGE_FILE  = basePath+sys.argv[3]

print "Output folder:"
print basePath+sys.argv[4]
saveToBasePath  = basePath+sys.argv[4]


if not os.path.isfile(PRETRAINED):
    print("PRETRAINED File not found")
else:
    print("PRETRAINED seems fine.")
    
if not os.path.isfile(DEPLOY_FILE):
    print("DEPLOY_FILE File not found")
else:
    print("DEPLOY_FILE seems fine.")
    
if not os.path.isfile(IMAGE_FILE):
    print("IMAGE_FILE File not found")
else:
    print("IMAGE_FILE seems fine.")


##### Initialize Caffe
caffe.set_mode_cpu()
net = caffe.Classifier(DEPLOY_FILE, PRETRAINED) #, image_dims=(28, 28)

##### Layer names and Blob sizes
feature_shape = [(k, v.data.shape) for k, v in net.blobs.items()]
print 
print "Feature shapes:"
print feature_shape

##### Parameter shapes
parameter_shape = [(k, v[0].data.shape) for k, v in net.params.items()]
print
print "Parameter shapes:"
print parameter_shape

##### Transforming of input
inputLayerName = feature_shape[0][0];
transformer = caffe.io.Transformer({inputLayerName: net.blobs[inputLayerName].data.shape})
transformer.set_transpose(inputLayerName, (2,0,1))
##transformer.set_mean('data', np.load(caffe_root + 'python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1)) # mean pixel
transformer.set_raw_scale(inputLayerName, 255)  # the reference model operates on images in [0,255] range instead of [0,1]
#transformer.set_channel_swap(inputLayerName, (2,1,0))  # the reference model has channels in BGR order instead of RGB

##### Forward pass
print
print "Forward pass"
net.blobs[inputLayerName].data[...] = transformer.preprocess(inputLayerName, caffe.io.load_image(IMAGE_FILE))
net.forward()


##### Print Prediction
number_of_layers = len(feature_shape)

print "Last layer for prediction:"
layerName = feature_shape[number_of_layers-1][0];
print layerName
     
feature = net.blobs[layerName].data[0][:36]
print "Output Layer is:"
print feature
print "Prediction is:"
print feature.argmax()

####Function for displaying several images in square
def vis_square(data, padsize=1, padval=0):
    data -= data.min()
    data /= data.max()
    
    # force the number of filters to be square
    n = int(np.ceil(np.sqrt(data.shape[0])))
    padding = ((0, n ** 2 - data.shape[0]), (0, padsize), (0, padsize)) + ((0, 0),) * (data.ndim - 3)
    data = np.pad(data, padding, mode='constant', constant_values=(padval, padval))
    
    # tile the filters into an image
    data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
    data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
#    print savePath+currentLayer+'.png'
    scipy.misc.imsave(savePath+currentLayer+'.png', data)





####### Save images
### Function for creating dirs
def createDirIfNotExists(name):
    if not os.path.exists(name):
        os.makedirs(name)



##### Save filters
import scipy
savePath=saveToBasePath+'filters/'
createDirIfNotExists(savePath)

print "Get and save Filters"
print "Trying to save filters to following directory:"
print savePath

currentLayer="conv1"
print "Saving " + currentLayer
filters = net.params[currentLayer][0].data
vis_square(filters.transpose(0, 2, 3, 1))

currentLayer="conv2"
print "Saving " + currentLayer
filters = net.params[currentLayer][0].data
vis_square(filters[:48].reshape(48**2, 5, 5))

currentLayer="conv3"
print "Saving " + currentLayer
filters = net.params[currentLayer][0].data
vis_square(filters[:256].reshape(256**2, 3, 3))

currentLayer="conv4"
print "Saving " + currentLayer
filters = net.params[currentLayer][0].data
vis_square(filters[:192].reshape(192**2, 3, 3))

currentLayer="conv5"
print "Saving " + currentLayer
filters = net.params[currentLayer][0].data
vis_square(filters[:192].reshape(192**2, 3, 3))
		
print "Saving of filters was successful"



##### Save Features

###Function for creating two dimensional array out of vector
def create2dimArr(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]        

savePath=saveToBasePath+'features/'
createDirIfNotExists(savePath)
print "Trying to save features to following directory:"
print savePath

number_of_layers = len(feature_shape)

print "Saving data layer"
scipy.misc.imsave(savePath+'data.png', transformer.deprocess('data', net.blobs['data'].data[0]))

for i in range(1, number_of_layers):
    currentLayer = feature_shape[i][0]
    print "Saving"
    print currentLayer
    feature = net.blobs[currentLayer].data[0]
    typeOfLayer=len(feature.shape)
#print typeOfLayer
#Data / Convolutional / Pooling
    if typeOfLayer == 3:
#        feature = net.blobs[currentLayer].data[0, :36]
#        print "is data /conv pool"
        vis_square(feature, padval=1)
#Fully connected:
    if typeOfLayer == 1:
#        print "is fully connected"
#        print len(feature)
        grayscale = feature*255
#        print grayscale
        newArr=create2dimArr(grayscale,64)
#        print newArr
        scipy.misc.imsave(savePath+currentLayer+'.png', newArr)

print "Saving of features was successful"

