#!/usr/bin/env sh
###
# Starte dieses script aus dem Startverzeichnis von caffe!
# Die relativen Pfade stimmen sonst nicht mehr.
###

# $1 dataSet
#/shared/studenten/nn_master_project/jonas/data/bottles-12/train
#/shared/studenten/nn_master_project/jonas/data/bottles-12/test
# $2 label-list
#/shared/studenten/nn_master_project/jonas/data/bottles-12/train_labels.txt
#/shared/studenten/nn_master_project/jonas/data/bottles-12/test_labels.txt
# $3 destination
#/shared/studenten/nn_master_project/jonas/data/bottles-12/bottles-12_train_lmdb
#/shared/studenten/nn_master_project/jonas/data/bottles-12/bottles-12_test_lmdb

echo "Creating mnist..."

RESIZE_HEIGHT=256
RESIZE_WIDTH=256

GLOG_logtostderr=1 /usr/local/caffe-master/build/tools/convert_imageset \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
	--shuffle \
	$1 \
	$2 \
	$3

echo "Done."
