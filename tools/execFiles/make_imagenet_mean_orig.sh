#!/usr/bin/env sh
# Compute the mean image from the imagenet training lmdb
# N.B. this is available in data/ilsvrc12

EXAMPLE=data
DATA=data/bottles
TOOLS=/usr/local/caffe-master/build/tools

GLOG_logtostderr=1 $TOOLS/compute_image_mean $EXAMPLE/bottles_test_lmdb \
  $DATA/bottles_mean.binaryproto

echo "Done."
