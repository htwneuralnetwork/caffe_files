#!/usr/bin/env sh
# Compute the mean image from the imagenet training lmdb
# N.B. this is available in data/ilsvrc12


TRAIN_LMDB=data/bottles-12/bottles-12_train_lmdb/
OUTPUT_FOLDER=data/bottles-12/
COMPUTE_IMAGEMEAN=/usr/local/caffe-master/.build_release/tools/compute_image_mean.bin

echo "Creating Train LMDB mean"

$COMPUTE_IMAGEMEAN \
$TRAIN_LMDB \
$OUTPUT_FOLDER/mean.binaryproto

echo "Done."
