/**
 * 
 */
package main;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Jonas_000
 *
 */
public class RenameImageFiles
{
	private static int renameCounter;
	private static String[] fillZeros;
	private static String format = ".jpg";
	/**
	 * @param args args[0] path to image folder
	 */
	public static void main(String[] args)
	{
		renameCounter = 0;
		format = "." + args[1];
		fillZeros = new String[] {"0000", "000", "00", "0"};
		
		if(args.length > 0)
		{
			File src = new File(args[0]);
			
			if(src.exists())
				renameFiles(src, args[2]);
			else
				System.out.println("ERROR: Not valid Path!");
				
			System.out.println("SUCCESS: " + renameCounter +" images renamed!");
		}
		else
			System.out.println("ERROR: No Path!");
	}
	
	private static void renameFiles(File directory, String args)
	{
		File[] files = directory.listFiles();
		
		for(int i = 0; i < files.length; ++i)
		{
			if(files[i].isDirectory())
				renameFiles(files[i], args);
			else
			{
				File actImg = files[i];
				String imgName = actImg.getName();
				String lowerCaseName = imgName.toLowerCase();
				
				if(lowerCaseName.endsWith(".png") || lowerCaseName.endsWith(".jpg"))
				{
					try 
					{
						Path source;
						
						String firstChar = args;// imgName.substring(0, 1);
						String rest = imgName.substring(2);
						
						int restZeros = String.valueOf(renameCounter).length() - 1;
						
						String newName = firstChar + "_" + fillZeros[restZeros] + renameCounter + format;
						
						source = Paths.get(actImg.getCanonicalPath());
						Files.move(source, source.resolveSibling(newName));
						
						++renameCounter;
					} catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		}
	}

}
