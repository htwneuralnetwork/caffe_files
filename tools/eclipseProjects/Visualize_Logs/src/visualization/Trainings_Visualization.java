/**
 * 
 */
package visualization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import com.sun.glass.ui.Robot;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

/**
 * @author lenny
 *
 */
public class Trainings_Visualization extends Application
{
	private static String firstLevel = "\t";
	private static String secondLevel = "\t\t";
	private static String thirdLevel = "\t\t\t";
	private static String fourthLevel = "\t\t\t\t";
	private static String separatingChar = ";";
	private static List<String> valueList;
	private static String fileName = "chart_";
	private static int chartCounter = 1;
	
	private static String imageFormat = ".png";
	private Stage primaryStage;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception
	{
		this.primaryStage = primaryStage;
		String[] args = this.getParameters().getRaw().toArray(new String[0]);
		String operation = args[0];
		
		switch (operation) 
		{
			case "help":
				printProgramInformation();
				break;
			case "visualize":
				checkVisualization(args);
				break;
			default:
				System.out.println(args[0] + " ist ein unbekannter Befehl!");
				break;
		}
		
		// Nach dem Zeichnen der Graphen soll das Programm beendet werden.
		System.exit(0);
	}
	
	private static void checkVisualization(String[] args)
	{
		System.out.println("\nExtract Trainingsvalues");
		
		File logDir = new File(args[1]);
		File outDir = new File(args[2]);
		
		if(!logDir.exists())
		{
			System.out.println("path not found: '" + args[1] + "'");
			return;
		}
		else if(!outDir.exists())
		{
			System.out.println("path not found: '" + args[2] + "'");
			return;
		}
		else if(logDir.isDirectory())
		{
			System.out.println(firstLevel + "read directory");
			//readDirectory(logDir, outDir, withVisualization);
		}
		else if(logDir.isFile())
		{
			System.out.println(firstLevel + "read file " + logDir.getName());
			String columnsToDraw = "";
			
			if(args.length > 3)
				columnsToDraw = args[3];
			
			valueList = readFile(logDir, outDir, columnsToDraw);
			visualizeExtractedDatas(valueList, outDir, columnsToDraw);
		}
		else
		{
			System.out.println("Error in CheckVisualization");
		}
	}
	
	private static List<String> readFile(File file, File outDirectory, String arg)
	{
		List<String> valueList = new ArrayList<String>();
		
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file)))
		{
			String sCurrentLine;
			
			while ((sCurrentLine = bufferedReader.readLine()) != null){
				valueList.add(sCurrentLine);
			}
			
		} catch (IOException e){
			e.printStackTrace();
		}
		
		return valueList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void visualizeExtractedDatas(List<String> valueList, File outputDirectory, String columnsToDraw)
	{
			String[][] datas = convertList2Array(valueList, separatingChar);
			String[] usedColumns;
			
			if(columnsToDraw == "")
				usedColumns = Arrays.copyOfRange(datas[0], 1, datas[0].length);
			else
				usedColumns = columnsToDraw.split(separatingChar);

			int[] seriesIndeces = getIndeces(usedColumns, datas[0]);
			
			final NumberAxis xAxis = new NumberAxis();
			final NumberAxis yAxis = new NumberAxis();
			xAxis.setLabel(datas[0][0]);
			yAxis.setLabel("Values");
			
			//TODO Den Namen der Tabelle noch richtig einbinden
			final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis); 
			lineChart.setAnimated(false);
			lineChart.setCreateSymbols(false);
			lineChart.setTitle("Trainings-Graph");
			
			for(int chart = 0; chart < seriesIndeces.length; ++chart)
			{
				int actColumn = seriesIndeces[chart];
				
				@SuppressWarnings("rawtypes")
				XYChart.Series series = new XYChart.Series();
				series.setName(datas[0][actColumn]);
				
				for(int row = 1; row < datas.length; ++row)
				{
					int xValue = Integer.valueOf(datas[row][0]);
					double yValue = Double.valueOf(datas[row][actColumn]);
					
					series.getData().add(new XYChart.Data(xValue, yValue));
				}
				
				lineChart.getData().add(series);
			}
			
			Scene scene = new Scene(lineChart, 800, 600);
			WritableImage snapShot = new WritableImage(800, 600);
			scene.snapshot(snapShot);
			
			/*for(int i = 0; i < usedColumns.length; ++i)
				fileName += usedColumns[i];
			*/
			
			String filePath = "";
			
			try
			{
				String outDir = outputDirectory.getCanonicalPath() + File.separatorChar;
				boolean isOk = false;
		
				while(!isOk)
				{
					String imageFullName = fileName + chartCounter + imageFormat;
					String tmpPath = outDir + imageFullName;
					
					if(new File(tmpPath).exists())
						++chartCounter;
					else
					{
						filePath = tmpPath;
						isOk = true;
					}
				}
				
			} catch (IOException e1){
				e1.printStackTrace();
			}
			
			try
			{
				ImageIO.write(SwingFXUtils.fromFXImage(snapShot, null), "png", new File(filePath));
				System.out.println(firstLevel + "image " + fileName + chartCounter + " saved");
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	private static int[] getIndeces(String[] listOne, String[] listTwo)
	{
		int[] indexList = new int[listOne.length];
		
		for(int i = 0; i < listOne.length; ++i)
		{
			for(int y = 0; y < listTwo.length; ++y)
			{
				if(listOne[i].equals(listTwo[y]))
					indexList[i] = y;
			}
		}
		
		return indexList;
	}
	
	private static String[][] convertList2Array(List<String> list, String separatorChar)
	{
		int columns = list.get(0).split(separatorChar).length;
		int rows = list.size();
		
		String[][] convertedData = new String[rows][];
		
		for(int i = 0; i < rows; ++i)
		{
			convertedData[i] = list.get(i).split(separatorChar);
		}
		
		return convertedData;
	}
	
	private static void printProgramInformation()
	{
		//TODO Ausgabe noch etwas uebersichtlicher gestalten.
		
		System.out.println("<------------------------------->");
		System.out.println("<------------ Infos ------------>");
		System.out.println();
		System.out.println(firstLevel  + "<---- Operationen ---->");
		System.out.println(firstLevel  + "  visualize \t \t logfile_visualization.jar visualize <logfile source> <destination directory> <whichColumn>");
		System.out.println(firstLevel  + "  help \t \t \t gibt Informationen �ber dieses Programm aus.");
		System.out.println();
		System.out.println(firstLevel  + "<---- Parameter ---->");
		System.out.println(firstLevel  + "  <logfile source> \t Der Pfad zur gefilterten Trainingsausgabe.");
		System.out.println(firstLevel  + "  <dst directory> \t Der Pfad zum Ordner, in dem die Bilder rein sollen.");
		System.out.println(firstLevel  + "  <whichColumn> \t Gibt an welche Spalten in dem Graphen kommen sollen.");
		System.out.println(fourthLevel + " 'true' fuer alle Bilder oder einzelne Spalten angeben.");
		System.out.println(fourthLevel + " Bsp: 'TrainingsLoss/ValidationLoss'");
		System.out.println();
		System.out.println("<-------- Infos - Ende --------->");
		System.out.println("<------------------------------->");
	}
}
