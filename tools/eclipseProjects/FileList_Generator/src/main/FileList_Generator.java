/**
 * 
 */
package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Jonas_000
 *
 */
public class FileList_Generator
{
	private static int labelsCount;
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException
	{
		labelsCount = 0;
		
		if(args.length > 0)
		{
			File srcDirectory = new File(args[0]);
			
			if(srcDirectory.exists() && srcDirectory.isDirectory())
			{
				String basePath;
				
				if(args.length > 1 && args[1].equals("absolute"))
					basePath = srcDirectory.getCanonicalPath();
				else
					basePath = "";
				
				createFile(srcDirectory, basePath);
				System.out.println("SUCCESS: " + labelsCount + " labels added!");
			}
			else
				System.out.println("ERROR: source is not a directory! " + args[0]);
		}
		else
			System.out.println("ERROR: invalid parameter count!");

	}

	private static void createFile(File srcDirectory, String basePath)
	{
		try {
			String directoryPath = srcDirectory.getCanonicalPath();
			String labelListPath = "";
			String outputFileName = srcDirectory.getName() + "_labels.txt";
			
			int lastSeparator = directoryPath.lastIndexOf(File.separator);
			labelListPath = directoryPath.substring(0, lastSeparator);
			
			File labelList = new File(labelListPath + File.separator + outputFileName);
			
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(labelList)))
			{
				writeLabels(srcDirectory, basePath, writer);
				writer.close();
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void writeLabels(File srcDirectory, String basePath, BufferedWriter writer)
	{
		File[] files = srcDirectory.listFiles();
		
		for(int i = 0; i < files.length; ++i)
		{
			if(files[i].isDirectory())
			{
				String modifiedBasePath = basePath + File.separator + files[i].getName(); 
				writeLabels(files[i], modifiedBasePath, writer);
			}
			else
			{
				File actImg = files[i];
				String imgName = actImg.getName();
				String lowerCaseName = imgName.toLowerCase();
				
				if(lowerCaseName.endsWith(".png") || lowerCaseName.endsWith(".jpg"))
				{
					if(imgName.contains("_"))
					{
						String label = imgName.split("_")[0];
						String imgPath = basePath + File.separator + imgName;
						String line = imgPath + " " + label;
						
						try
						{
							writer.write(line);
							writer.newLine();
							++labelsCount;
						} catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
			}
		}
	}

}
