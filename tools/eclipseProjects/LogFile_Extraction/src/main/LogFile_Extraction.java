package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LogFile_Extraction 
{
	private static String outputFormat = ".txt";
	private static String logFormat = ".trainlog";
	
	private static String firstLevel = "\t";
	private static String secondLevel = "\t\t";
	private static String thirdLevel = "\t\t\t";
	private static String fourthLevel = "\t\t\t\t";
	private static String separatingChar = ";";
	private static List<String> valueList;
	
	public static void main(String[] args)
	{
		switch (args[0]) 
		{
			case "help":
				printProgramInformation();
				break;
			case "extract":
				extractValuesFromLogs(args);
				break;
			default:
				System.out.println(args[0] + " ist ein unbekannter Befehl!");
				break;
		}
	}
	
	private static void extractValuesFromLogs(String[] args)
	{
		System.out.println("\nExtract Trainingsvalues");
		
		File logDir = new File(args[1]);
		File outDir = new File(args[2]);
		
		if(!logDir.exists())
		{
			System.out.println("logDir not found: '" + args[0] + "'");
			return;
		}
		else if(!outDir.exists())
		{
			System.out.println("outDir not found: '" + args[1] + "'");
			return;
		}
		else if(logDir.isDirectory())
		{
			System.out.println(firstLevel + "read directory");
			readDirectory(logDir, outDir);
		}
		else
		{
			System.out.println(firstLevel + "read file");
			readFile(logDir, outDir);
		}
	}
	
	private static void readDirectory(File directory, File outDirectory)
	{
		File[] logfiles = directory.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(logFormat);
			}
		});
		
		for(int i = 0; i < logfiles.length; ++i)
			readFile(logfiles[i], outDirectory);
	}
	
	private static void readFile(File file, File outDirectory)
	{
		valueList = new ArrayList<String>();
		valueList.add("Iteration;TrainLoss;TrainLR;ValidationLoss;ValidationAcc");
		
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file)))
		{
			boolean isTraining = false;
			String sCurrentLine;
			String actIteration = null;
			String actLearningrate = null;
			String actTrainingsLoss = null;
			String actValidationLoss = null;
			String actValidationAccuracy = null;
			
			while ((sCurrentLine = bufferedReader.readLine()) != null) 
			{
				if(sCurrentLine.contains("Solver scaffolding done"))
					isTraining = true;
				
				if(isTraining)
				{
					// ueberpruefe ob es immer noch die gleiche Iteration ist
					if(sCurrentLine.contains("Iteration"))
					{
						String[] tempArray = sCurrentLine.split("Iteration");
						String tmpIteration = tempArray[1].split(",")[0];
						tmpIteration = tmpIteration.replace(" ", "");
						
						if(actIteration != null && !actIteration.equals(tmpIteration))
						{
							valueList.add(actIteration + separatingChar + 
										  actTrainingsLoss + separatingChar +
										  actLearningrate + separatingChar +
										  actValidationLoss + separatingChar + 
										  actValidationAccuracy);
							
							actIteration = tmpIteration;
						}
						else{
							actIteration = tmpIteration;
						}
					}
					
					// ueberpruefe ob die aktuelle Zeile den Trainingsloss hat
					if(sCurrentLine.contains("Iteration") && sCurrentLine.contains("loss"))
					{
						String[] tmpArray = sCurrentLine.split("loss =");
						String tmpTrainingsLoss = tmpArray[1].split(" ")[1];
						actTrainingsLoss = tmpTrainingsLoss;
					}
					
					// ueberpruefe ob die aktuelle Zeile die Lernrate hat
					if(sCurrentLine.contains("lr"))
					{
						String[] tmpArray = sCurrentLine.split("lr =");
						String tmpLearningrate = tmpArray[1].split(" ")[1];
						actLearningrate = tmpLearningrate;
					}
					
					// ueberpruefe ob die aktuelle Zeile den Validationloss hat
					if(sCurrentLine.contains("Test net") && sCurrentLine.contains("accuracy"))
					{
						String[] tmpArray = sCurrentLine.split(" ");
						String tmpValidationAccuracy = tmpArray[tmpArray.length - 1];
						actValidationAccuracy = tmpValidationAccuracy;
					}
					
					// ueberpruefe ob die aktuelle Zeile die ValidationAccuracy hat
					if(sCurrentLine.contains("Test net") && sCurrentLine.contains("loss"))
					{
						String[] tmpArray = sCurrentLine.split("loss =");
						String tmpValidationLoss= tmpArray[1].split(" ")[1];
						actValidationLoss = tmpValidationLoss;
					}
				}
			}
			
			// Die letzten Werte werden nicht mehr ueber die while-Schleife eingetragen, das muss hier selber nochmal gemacht werden.
			valueList.add(actIteration + separatingChar + 
					  actTrainingsLoss + separatingChar +
					  actLearningrate + separatingChar +
					  actValidationLoss + separatingChar + 
					  actValidationAccuracy + separatingChar);
 
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		// Die Liste muss jetzt in 
		writeFile(valueList, outDirectory, file.getName());
	}
	
	private static void writeFile(List<String> values, File outputDirectory, String fileName)
	{
		try {
			String completePath;
			String outputFileName = "/" + fileName.split(".trainlog")[0] + "_filtered";
			
			System.out.println(firstLevel + "write file '" + outputFileName + "' to '" + outputDirectory + "'");
			completePath = outputDirectory.getCanonicalPath() + outputFileName + outputFormat;
		
			File valuesFile = new File(completePath);
			
			try(BufferedWriter writer = new BufferedWriter(new FileWriter(valuesFile)))
			{
				for(int i = 0; i < values.size(); ++i)
				{
					writer.write(values.get(i));
					writer.newLine();
				}
				
				writer.close();
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void printProgramInformation()
	{
		//TODO Ausgabe noch etwas uebersichtlicher gestalten.
		
		System.out.println("<------------------------------->");
		System.out.println("<------------ Infos ------------>");
		System.out.println();
		System.out.println(firstLevel  + "<---- Operationen ---->");
		System.out.println(firstLevel  + "  extract \t \t logfile_extraction.jar extract <logfile source> <destination directory>");
		System.out.println(firstLevel  + "  help \t \t \t gibt Informationen �ber dieses Programm aus.");
		System.out.println();
		System.out.println(firstLevel  + "<---- Parameter ---->");
		System.out.println(firstLevel  + "  <logfile source> \t Der Pfad zur Datei in der die Trainingsausgabe gespeichert wurde.");
		System.out.println(firstLevel  + "  <destination path> \t Der Pfad zum Ordner, in dem die ausgelesenen Werte gespeichert werden sollen.");
		System.out.println(firstLevel  + "  <withVisualization> \t Flag ob die ausgelesenen Werte in einem Bild dargestellt werden sollen.");
		System.out.println(secondLevel + "\t \t 'true' zeigt im Bild alle Spalten an oder die gewuenschten Spalten angeben bsp: 'TrainingsLoss/ValidationLoss'");
		System.out.println();
		System.out.println("<-------- Infos - Ende --------->");
		System.out.println("<------------------------------->");
	}
}
